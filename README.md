## Setup a React Environment Using webpack and Babel

When I was trying to learn React, one of the things that bothered me was that I couldnt find a quick way to get started with simple setup so I thought I'll create a boiler plate with React, Webpack and blueprint.js

#### Requirements:

 * git
 * Node v6
 * npm v3


#### Get Started

 * git clone this repo or download as zip
 * cd into the directory where this repo is cloned
 * Install dependencies by executing npm install
  ```
 $ yarn
 ```
 or
 ```
 $ npm install
 ```
 #

 * Run web pack server
```
 $ yarn start
```
 or
```
 $ npm start
```
 * Pull up a browser and navigate to http://localhost:8080
