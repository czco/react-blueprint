import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import 'normalize.css/normalize.css';
import '@blueprintjs/core/dist/blueprint.css';
// extract specific components
import { Intent, Spinner } from "@blueprintjs/core";
// or just take everything!
import * as Blueprint from "@blueprintjs/core";


ReactDOM.render(<App />, document.getElementById('root'));
